//http://www.codeabbey.com/index/task_view/collatz-sequence
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int countIters(int start)
{
    int count = 0, value = start;
    while (value != 1)
    {
        if (value % 2) // odd
            value = 3 * value + 1;
        else // even
            value = value / 2;
        count++;
    }
    return count;
}

int main()
{
    int numTimes;
    std::cin >> numTimes;
    int start;
    for (int i = 0; i < numTimes; i++)
    {
        std::cin >> start;
        std::cout << countIters(start) << " ";
    }
}
