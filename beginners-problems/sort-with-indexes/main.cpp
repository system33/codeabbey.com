//http://www.codeabbey.com/index/task_view/sort-with-indexes
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int find(int* haystack, int haystackSize, int needle)
{
    for (int i = 0; i < haystackSize; i++)
    {
        if (needle == haystack[i]) return i+1;
    }
    return -1;
}

void bubbleSort(int* array, int size)
{
    int buffer;
    bool swappedOne = false;
    for (; size >= 1; size--)
    {
        swappedOne = false;
        for (int i = 0; i < size-1; i++)
        {
            if (array[i] > array[i+1])
            {
                buffer = array[i+1];
                array[i+1] = array[i];
                array[i] = buffer;
                swappedOne = true;
            }
        }
        if (swappedOne == false) break;
    }
}

int main()
{
    int ars;
    std::cin >> ars;
    int arrInit[ars],arrFinal[ars];
    for (int i = 0; i < ars; i++)
    {
        std::cin >> arrInit[i];
        arrFinal[i] = arrInit[i];
    }
    bubbleSort(arrFinal, ars);
    for (int i = 0; i < ars; i++)
        std::cout << find(arrInit, ars, arrFinal[i]) << " ";

}
