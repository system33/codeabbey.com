//http://www.codeabbey.com/index/task_view/greatest-common-divisor
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int greatestCommonDivisor(int a, int b)
{
    if      (a > b)  {return greatestCommonDivisor(a-b, b);}
    else if (a < b)  {return greatestCommonDivisor(b  , a);}
    else return a;
}

int main()
{
    int numTimes;
    std::cin >> numTimes;
    int a,b,gcd,lcm;
    for (int i = 0; i < numTimes; i++)
    {
        std::cin >> a >> b;
        gcd = greatestCommonDivisor(a,b);
        lcm = a*b/gcd;
        std::cout << "(" << gcd << " " << lcm << ") ";
    }
}
