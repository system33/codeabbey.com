//http://www.codeabbey.com/index/task_view/array-checksum
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int main()
{
    int size;
    std::cin >> size;
    long result;
    const int seed = 113;
    const int modulo = 10000007;
    int tmp;
    for (int i = 0; i < size; i++)
    {
        std::cin >> tmp;
        result = ((result + tmp) * seed) % modulo;
    }
    std::cout << result;
}
