//http://www.codeabbey.com/index/task_view/modular-calculator
//https://bitbucket.org/system33/beginners-problems
#include <iostream>
#include <string>

int main()
{
    long total;
    std::cin >> total;
    std::string operation;
    std::cin >> operation;
    int tmp;
    while (operation != "%")
    {
        std::cin >> tmp;
        if (operation == "+") total += tmp;
        else                  total *= tmp;
        std::cin >> operation; //get next one
    }
    std::cin >> tmp;
    std::cout << total % tmp;
}
