//http://www.codeabbey.com/index/task_view/array-counters
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int main()
{
    int entries, range, tmp;
    std::cin >> entries >> range;
    int arr[range];
    for (int i = 0; i < range; i++) arr[i] = 0;
    for (int i = 0; i < entries; i++)
    {
        std::cin >> tmp;
        arr[tmp-1]++;
    }
    for (int i = 0; i < range; i++)
    {
        std::cout << arr[i] << " ";
    }
}
