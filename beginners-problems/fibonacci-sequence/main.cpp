//http://www.codeabbey.com/index/task_view/fibonacci-sequence
//https://bitbucket.org/system33/beginners-problems
#include <iostream>
#include <string>
#include <vector>



int char2int(const char c) {return c-'0';}
char int2char(const int i) {return i+'0';}

class BigNum
{
public:
    std::string get() {return number;}
    void set(std::string num) {number = num;}
    BigNum operator+(BigNum b)
    {
        // create two arrays big enough to hold the biggest number
        const unsigned int maxdigits = (get().length() > b.get().length() ? get().length() : b.get().length());
        int arr1[maxdigits];
        int arr2[maxdigits];
        // fill with zeros
        for (unsigned int i = 0; i < maxdigits; i++) {arr1[i] = 0; arr2[i] = 0;}
        // put the numbers into the arrays backwards
        // this guarantees alignment of each digit
        for (unsigned int i = 0; i < get().length(); i++)
            arr1[get().length()-1-i] = char2int(get()[i]);
        for (unsigned int i = 0; i < b.get().length(); i++)
            arr2[b.get().length()-1-i] = char2int(b.get()[i]);

        int carry = 0;
        int ans[maxdigits+1]; // extra spot for possible carry
        for (unsigned int i = 0; i < maxdigits+1; i++) ans[i] = 0;
        for (unsigned int i = 0; i < maxdigits; i++)
        {
            int sum = arr1[i] + arr2[i] + carry;
            carry = 0;
            while (sum >= 10)
            {
                carry++;
                sum -= 10;
            }
            ans[i] = sum;
        }
        ans[maxdigits+1-1] = carry; // +1-1 silly, but indicates intent better
        std::string answer;
        for (int i = maxdigits+1-1; i >= 0; i--)
        {
            answer += int2char(ans[i]);
        }
        while (answer[0] == '0') answer = answer.substr(1, std::string::npos);
        return BigNum(answer);
    }
    BigNum() {number = "0";}
    BigNum(std::string num) {number = num;}
private:
    std::string number;
};

int find(std::vector<BigNum>* haystack, BigNum needle)
{
    for (unsigned int i = 0; i < haystack->size(); i++)
        {if (needle.get() == haystack->at(i).get()) return i;}
    return -1; // indicates not found
}

int main()
{
    std::vector<BigNum> fibonacci;
    fibonacci.push_back(BigNum("0"));
    fibonacci.push_back(BigNum("1"));
    int numTimes;
    std::cin >> numTimes;
    BigNum target;
    std::string tmp;
    int indice;
    for (int i = 0; i < numTimes; i++)
    {
        std::cin >> tmp;
        target = BigNum(tmp);
        indice = find(&fibonacci, target);
        if (indice < 0) // not found
        {
            do
            {
                BigNum* prev = &fibonacci.at(fibonacci.size()-1);
                BigNum* prev2 = &fibonacci.at(fibonacci.size()-2);
                fibonacci.push_back(*prev + *prev2);
            } while (fibonacci.at(fibonacci.size()-1).get() != target.get());
            indice = fibonacci.size()-1;
        }
        std::cout << indice << " ";
    }
    return 0;
}
