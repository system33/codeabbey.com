//http://www.codeabbey.com/index/task_view/maximum-of-array
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int main()
{
    const int arraySize = 300;
    int tmp,min,max;
    for (int i = 0; i < arraySize; i++)
    {
        std::cin >> tmp;
        if (i == 0) min = max = tmp;
        else if (tmp < min) min = tmp;
        else if (tmp > max) max = tmp;
    }
    std::cout << max << " " << min;
    return 0;
}
