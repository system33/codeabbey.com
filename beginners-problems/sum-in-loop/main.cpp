//http://www.codeabbey.com/index/task_view/sum-in-loop
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int main()
{
    int numVals;
    std::cin >> numVals;
    int total = 0, tmp;
    for (int i = 0; i < numVals; i++)
    {
        std::cin >> tmp;
        total += tmp;
    }
    std::cout << total;
    return 0;
}
