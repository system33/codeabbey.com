//http://www.codeabbey.com/index/task_view/weighted-sum-of-digits
//https://bitbucket.org/system33/beginners-problems
#include <iostream>
#include <string>

int char2int(char c)
{
    switch(c)
    {
        case '1': return 1; break;
        case '2': return 2; break;
        case '3': return 3; break;
        case '4': return 4; break;
        case '5': return 5; break;
        case '6': return 6; break;
        case '7': return 7; break;
        case '8': return 8; break;
        case '9': return 9; break;
        default: return 0;
    }
}

int wsd(std::string str)
{
    int total = 0;
    for (unsigned int i = 0; i < str.length(); i++)
    {
        total += char2int(str[i]) * (i+1);
    }
    return total;
}

int main()
{
    int numTimes;
    std::cin >> numTimes;
    std::string value;
    for (int i = 0; i < numTimes; i++)
    {
        std::cin >> value;
        std::cout << wsd(value) << " ";
    }
}
