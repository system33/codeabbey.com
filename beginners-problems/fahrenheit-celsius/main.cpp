//http://www.codeabbey.com/index/task_view/fahrenheit-celsius
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int round(double value)
{
    bool neg = value < 0;
    if (neg) value *= -1;
    if (value - (int)value >= .5) value++;
    if (neg) value *= -1;
    return (int)value;
}

int main()
{
    int n;
    std::cin >> n;
    int fahrenheit;
    for (int i = 0; i < n; i++)
    {
        std::cin >> fahrenheit;
        std::cout << round((fahrenheit - 32) * 5 / (double)9) << " ";
    }
    return 0;
}
