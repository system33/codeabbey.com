//http://www.codeabbey.com/index/task_view/sums-in-loop
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int main()
{
    int numLoops;
    std::cin >> numLoops;
    int a,b;
    for (int i = 0; i < numLoops; i++)
    {
        std::cin >> a >> b;
        std::cout << a+b << " ";
    }
    return 0;
}
