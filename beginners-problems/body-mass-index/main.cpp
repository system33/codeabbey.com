//http://www.codeabbey.com/index/task_view/body-mass-index
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int main()
{
    int numTimes;
    std::cin >> numTimes;
    double weight, height, bmi;
    for (int i = 0; i < numTimes; i++)
    {
        std::cin >> weight >> height;
        bmi = weight / height / height;
        if      (bmi < 18.5) std::cout << "under";
        else if (bmi < 25.0) std::cout << "normal";
        else if (bmi < 30.0) std::cout << "over";
        else                 std::cout << "obese";
        std::cout << " ";
    }
    return 0;
}
