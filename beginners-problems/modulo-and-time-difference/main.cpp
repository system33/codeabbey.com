//http://www.codeabbey.com/index/task_view/modulo-and-time-difference
//https://bitbucket.org/system33/beginners-problems
#include <iostream>
#include <string>

class Date
{
private:
    unsigned int days()    {return value              / (60*60*24);}
    unsigned int hours()   {return value % (60*60*24) / (60*60   );}
    unsigned int minutes() {return value % (60*60   ) / (60      );}
    unsigned int seconds() {return value % (60      )             ;}
    unsigned int value;
public:
    void print()
    {
        //std::cout << value << std::endl;
        std::cout << "(" << days() << " " << hours() << " " << minutes() << " " << seconds() << ")";
    }
    unsigned int get() {return value;}
    void set(unsigned int val) {value = val;}
    Date(unsigned int d, unsigned int h, unsigned int m, unsigned int s) {value = d*(60*60*24)+h*(60*60)+m*(60)+s;}
    Date(unsigned int s = 0)                                             {value =                               s;}
};

int main()
{
    int numTimes;
    std::cin >> numTimes;
    Date date1,date2,diff;
    unsigned int d,h,m,s;
    for (int i = 0; i < numTimes; i++)
    {
        std::cin >> d >> h >> m >> s;
        date1 = Date(d,h,m,s);
        std::cin >> d >> h >> m >> s;
        date2 = Date(d,h,m,s);
        diff = Date(date2.get() - date1.get());
        diff.print();
        std::cout << " ";
    }
    return 0;
}
