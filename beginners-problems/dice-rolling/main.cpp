//http://www.codeabbey.com/index/task_view/dice-rolling
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int toDice(double val, int sides)
{
    // return value cast to an int

    //return val*sides;   // 0 - 5 for six sided die
    return val*sides+1; // 1 - 6
}

int main()
{
    int numTimes;
    std::cin >> numTimes;
    for (int i = 0; i < numTimes; i++)
    {
        double randValue;
        std::cin >> randValue;
        std::cout << toDice(randValue, 6) << " ";
    }
}
