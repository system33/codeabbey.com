//http://www.codeabbey.com/index/task_view/rounding
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int round(double value)
{
    bool neg = value < 0;
    if (neg) value *= -1;
    if (value - (int)value >= .5) value++;
    if (neg) value *= -1;
    return (int)value;
}

int main()
{
    int numPairs;
    std::cin >> numPairs;
    int a,b;
    for (int i = 0; i < numPairs; i++)
    {
        std::cin >> a >> b;
        std::cout << round(a / (double)b) << " ";
    }
    return 0;
}
