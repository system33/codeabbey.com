//http://www.codeabbey.com/index/task_view/neumanns-random-generator
//https://bitbucket.org/system33/beginners-problems
#include <iostream>
#include <vector>

bool find(std::vector<int>* set, int val)
{
    for (unsigned int i = 0; i < set->size(); i++)
        if (val == set->at(i))
            return true;
    return false;
}

int nextNeumann(long val)
    {return ((val*val)/100)%10000;}

int main()
{
    int numTimes;
    std::cin >> numTimes;
    for (int i = 0; i < numTimes; i++)
    {
        int value, count = 0;
        std::vector<int> set;
        std::cin >> value;
        do
        {
            set.push_back(value);
            value = nextNeumann(value);
            count++;

        } while (!find(&set, value));
        std::cout << count << " ";
    }
}
