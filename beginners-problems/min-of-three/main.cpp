//http://www.codeabbey.com/index/task_view/min-of-three
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int main()
{
    int numTriplets;
    std::cin >> numTriplets;
    int a,b,c;
    for (int i = 0; i < numTriplets; i++)
    {
        std::cin >> a >> b >> c;
        if (a < b && a < c)
            std::cout << a;
        else if  (b < c)
            std::cout << b;
        else
            std::cout << c;
        std::cout << " ";
    }
    return 0;
}
