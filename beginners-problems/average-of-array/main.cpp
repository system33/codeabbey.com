//http://www.codeabbey.com/index/task_view/average-of-array
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int round(double value)
{
    bool neg = value < 0;
    if (neg) value *= -1;
    if (value - (int)value >= .5) value++;
    if (neg) value *= -1;
    return (int)value;
}

int main()
{
    int numTimes;
    std::cin >> numTimes;
    for (int i = 0; i < numTimes; i++)
    {
        int total = 0, count = 0, tmp;
        while (std::cin >> tmp)
        {
            if (tmp == 0) break;
            else
            {
                total += tmp;
                count ++;
            }
        }
        std::cout << round(total / (double)count) << " ";
    }
}
