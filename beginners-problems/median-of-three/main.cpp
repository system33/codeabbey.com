//http://www.codeabbey.com/index/task_view/median-of-three
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

void sort(int* arr, int ars)
{
    int buffer;
    for (int i = 0; i < ars - 1; i++)
    {
        if (arr[i] > arr[ars-1])
        {
            buffer = arr[ars-1];
            arr[ars-1] = arr[i];
            arr[i] = buffer;
        }
    }
    if (ars > 0) sort(arr, ars-1);
}

int main()
{
    int numGroups;
    std::cin >> numGroups;
    int arr[3];
    for (int i = 0; i < numGroups; i++)
    {
        std::cin >> arr[0] >> arr[1] >> arr[2];
        sort(arr, 3);
        std::cout << arr[1] << " ";
    }
}
