//http://www.codeabbey.com/index/task_view/bubble-sort
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int main()
{
    int size;
    std::cin >> size;
    int arr[size];
    for (int i = 0; i < size; std::cin >> arr[i++]);
    int passes = 0, swaps = 0, lastSwaps = 0, tmp;
    do
    {
        lastSwaps = swaps;
        for (int i = 0; i < size - 1; i++)
        {
            if (arr[i] > arr[i+1])
            {
                tmp = arr[i+1];
                arr[i+1] = arr[i];
                arr[i] = tmp;
                swaps++;
            }
        }
        size--;
        passes++;
    } while (lastSwaps != swaps);
    std::cout << passes << " " << swaps;
}
