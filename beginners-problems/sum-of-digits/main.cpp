//http://www.codeabbey.com/index/task_view/sum-of-digits
//https://bitbucket.org/system33/beginners-problems
#include <iostream>
#include <string>

int sumDigits(int val)
{
    if (val/10 == 0)
        return val % 10;
    else
        return (val % 10) + sumDigits(val/10);
}

int main()
{
    int numTimes;
    std::cin >> numTimes;
    int a,b,c,value;
    for (int i = 0; i < numTimes; i++)
    {
        std::cin >> a >> b >> c;
        value = a * b + c;
        std::cout << sumDigits(value) << " ";
    }
}
