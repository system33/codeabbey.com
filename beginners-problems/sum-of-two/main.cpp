//http://www.codeabbey.com/index/task_view/sum-of-two
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int main()
{
    int num1, num2;
    std::cin >> num1 >> num2;
    std::cout << num1+num2;
    return 0;
}
