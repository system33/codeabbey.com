//http://www.codeabbey.com/index/task_view/vowel-count
//https://bitbucket.org/system33/beginners-problems
#include <iostream>
#include <string>

// custom find function, unused but worked for this problem
// opted to use string::find instead, as it is built-in
/*bool find(char needle, std::string haystack)
{
    for (unsigned int i = 0; i < haystack.length(); i++)
        if (needle == haystack[i]) return true;
    return false;
}*/

int main()
{
    int lines;
    std::cin >> lines;
    std::cin.ignore(10000, '\n');
    std::string vowels = "aeiouy";
    std::string line;
    for (int i = 0; i < lines; i++)
    {
        getline(std::cin, line);
        int vowelCount = 0;
        for (unsigned int i = 0; i < line.length(); i++)
        {
            if (vowels.find(line[i]) != std::string::npos) vowelCount++;
        }
        std::cout << vowelCount << " ";
    }
    return 0;
}
