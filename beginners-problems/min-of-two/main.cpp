//http://www.codeabbey.com/index/task_view/min-of-two
//https://bitbucket.org/system33/beginners-problems
#include <iostream>

int main()
{
    int numPairs;
    std::cin >> numPairs;
    int a,b;
    for (int i = 0; i < numPairs; i++)
    {
        std::cin >> a >> b;
        std::cout << (a < b ? a : b) << " ";
    }
    return 0;
}
